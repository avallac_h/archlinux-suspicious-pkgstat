""" Shared
"""

import csv
import site
import os
import sys

from dataclasses import dataclass


@dataclass
class Packager:
    standsWithUkraine: bool = False
    name: str = ""
    givenName: str = ""
    familyName: str = ""
    jobTitle: str = ""
    alias: str = ""
    email: str = ""
    otherContact: str = ""
    pgpKey: str = ""
    roles: str = ""
    website: str = ""
    occupation: str = ""
    birthYear: str = ""
    country: str = ""
    location: str = ""
    languages: str = ""
    interests: str = ""
    favoriteDistros: str = ""
    proofURL1: str = ""
    proofURL2: str = ""
    proofURL3: str = ""
    proofURL4: str = ""


def dumpcsv(data: list, outfile: str):
    with open(outfile, "w") as fp:
        csv_writer = csv.writer(fp, delimiter=",", quotechar='"', lineterminator="\n")

        header = data[0].__dict__.keys()
        csv_writer.writerow(header)

        for packager in data:
            row = packager.__dict__.values()
            csv_writer.writerow(row)


def loadcsv(infile: str):
    data = []
    with open(infile, "r") as fp:
        reader = csv.reader(fp, delimiter=",", quotechar='"')
        header: list = next(reader, [])

        for row in reader:
            p = Packager()

            for idx, attr in enumerate(header):
                if attr == "standsWithUkraine":
                    if row[idx].lower() == "true":
                        setattr(p, attr, True)
                    else:
                        setattr(p, attr, False)
                else:
                    setattr(p, attr, row[idx])

            data.append(p)

    return data


def finddatafile(path: str):
    name = os.path.basename(path)

    # current directory
    guess = os.path.join(os.getcwd(), name)
    if os.path.isfile(guess):
        return guess

    # upper directory 
    # ../*.csv - running without installation
    guess = os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))), name
    )
    if os.path.isfile(guess):
        return guess

    # user home directory
    # $HOME/.local/share/asps/*.csv - installed with `pip --user`
    guess = os.path.join(site.USER_BASE, path)
    if os.path.isfile(guess):
        return guess

    # egg directory 
    # ../share/asps/*.csv - installed via setup.py in virtualenv
    guess = os.path.join(os.path.dirname(os.path.dirname(__file__)), path)
    if os.path.isfile(guess):
        return guess

    # system /etc directory
    # /etc/asps/*.csv - arch package
    guess = os.path.join("/etc/asps", name)
    if os.path.isfile(guess):
        return guess

    # system directory
    # /usr/share/asps/*.csv - normal sudo installation
    guess = os.path.join(sys.prefix, path)
    if os.path.isfile(guess):
        return guess

    raise RuntimeError("Cannot find data file: %s" % path)

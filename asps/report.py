#!/usr/bin/env python

import argparse
import os
import re
import time
import subprocess
import sys

from asps import finddatafile, loadcsv

# Optional color support
try:
    from colorama import Fore, Style, init

    USE_COLOR = True
except ImportError:
    USE_COLOR = False


def parse_arguments():
    """Parse command-line arguments"""
    parser = argparse.ArgumentParser(
        description="Check installed Arch Linux packages and report about "
        + "suspicious ones created by russian packagers"
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="be verbose (can be used multiple times to increase verbosity)",
        action="count",
        default=0,
    )
    parser.add_argument(
        "-n",
        "--no-unknown",
        help="""
            don't report about packages created by Unknown Packager (AUR,
            self-built ones)
            """,
        action="store_true",
        default=False,
    )

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-a",
        "--all-packages",
        help="""
            report all packages, not only installed on the system
            """,
        action="store_true",
        default=False,
    )
    group.add_argument(
        "-u",
        "--upgrades",
        help="""
            report only about outdated (upgradable) packages
            """,
        action="store_true",
        default=False,
    )
    group.add_argument(
        "-p",
        "--pacman-hook",
        help="""
            act as pre-install pacman hook (read package names to check from STDIN,
            one name per line)
            """,
        action="store_true",
        default=False,
    )

    return parser.parse_args()


def is_trusted(packager: str, packagers: list) -> int:
    """Check if packager is russian or untrusted
    Returns:
        int: return code:
            2  : packager is trusted (from CSV data)
            1  : packager is not russian
            0  : packager is russian
            -1 : packager not found in trusted list
    """
    match = None

    for p in packagers:
        if p.name.title() == packager.title():
            match = p
            break

    result = None

    if not match:
        result = -1
    elif match.standsWithUkraine:
        result = 2
    elif (
        re.search(r"Russia", match.country, re.IGNORECASE)
        or re.search(r"Russia", match.location, re.IGNORECASE)
        or re.search(r"Russian", match.languages, re.IGNORECASE)
    ):
        result = 0
    else:
        result = 1

    assert isinstance(result, int), 'Cannot determine trust for "{}"'.format(packager)

    return result


def get_newest_file(path: str, recursive=False, by="mtime"):
    """Get filename of newest file in the directory
    Args:
        path (str):       Path to directory to look into.
        recursive (bool): Process subdirectories.
        by (str):         One of these: 'ctime', 'mtime', 'atime'.
    Returns:
        Path to file or None.
    """
    by_opts = ["ctime", "mtime", "atime"]
    if by not in by_opts:
        raise ValueError("Invalid 'by' value. Expected one of: %s" % by_opts)

    result = None
    files = []

    if recursive:
        for dirpath, _, filenames in os.walk(path):
            for f in filenames:
                files.append(os.path.join(dirpath, f))

    else:
        files = os.listdir(path)
        files = [os.path.join(path, f) for f in files]
        files = list(filter(os.path.isfile, files))

    if not files:
        return result

    if by == "ctime":
        result = max(files, key=os.path.getctime)
    if by == "mtime":
        result = max(files, key=os.path.getmtime)
    if by == "atime":
        result = max(files, key=os.path.getatime)

    return result


def parse_pacman_output(output: bytes, verbosity=0, progress=True) -> dict:
    """Parse pacman output and return dictionary
    Args:
        output (bytes): pacman's stdout
    Returns:
        Dictionary like {pkgname: packager, ...}
    """
    # Define regexes
    re_pkgname = re.compile(r"^Name\s*:\s+(.*?)\s*$", re.DOTALL)
    re_packager = re.compile(r"^Packager\s*:\s+(.*?)(?:\s+[\(<].*?)?\s*$", re.DOTALL)

    # Parse output
    result = {}
    pkgname = None
    packager = None
    qty = 0

    for line in output.decode().split("\n"):
        match = re.search(re_pkgname, line)
        if match:
            pkgname = match.group(1)
            if verbosity > 2:
                print("{}\r[d] pkgname:  {}".format(80 * " ", pkgname))

        match = re.search(re_packager, line)
        if match:
            assert pkgname, "Parsing error."
            packager = match.group(1)
            if verbosity > 2:
                print("{}\r[d] packager: {}".format(80 * " ", packager))

            result[pkgname] = packager

            pkgname = None
            packager = None
            qty += 1

        # Display progress
        if progress:
            if not qty % 4:
                print(
                    "Read {} packages.".format(qty),
                    end="\r",
                    file=sys.stderr,
                    flush=True,
                )

    return result


class Msg:
    def __init__(self):
        if USE_COLOR:
            init()  # colorama.init()

    @staticmethod
    def packager_has_changed(pkgname: str, old_packager: str, new_packager: str):
        print(
            '{}{}{} has changed it\'s packager: "{}{}{}" -> "{}{}{}"'.format(
                Style.BRIGHT if USE_COLOR else "",
                pkgname,
                Style.RESET_ALL if USE_COLOR else "",
                Fore.GREEN if USE_COLOR else "",
                old_packager,
                Style.RESET_ALL if USE_COLOR else "",
                Fore.GREEN if USE_COLOR else "",
                new_packager,
                Style.RESET_ALL if USE_COLOR else "",
            ),
            flush=True,
        )

    @staticmethod
    def created_by_not_trusted(pkgname: str, packager: str):
        print(
            '{}{}{} is created by not trusted packager "{}{}{}"'.format(
                Style.BRIGHT + Fore.YELLOW if USE_COLOR else "",
                pkgname,
                Style.RESET_ALL if USE_COLOR else "",
                Fore.CYAN if USE_COLOR else "",
                packager,
                Style.RESET_ALL if USE_COLOR else "",
            ),
            flush=True,
        )

    @staticmethod
    def created_by_russian(pkgname: str, packager: str):
        print(
            '{}{}{} is created by russian packager "{}{}{}"'.format(
                Style.BRIGHT + Fore.RED if USE_COLOR else "",
                pkgname,
                Style.RESET_ALL if USE_COLOR else "",
                Fore.YELLOW if USE_COLOR else "",
                packager,
                Style.RESET_ALL if USE_COLOR else "",
            ),
            flush=True,
        )

    @staticmethod
    def verbose(pkgname: str, packager: str, result: int):
        print("[v] {:22s}\t{:38s}\t{:3.0f}".format(pkgname, packager, result))

    @staticmethod
    def total(suspicious: int, total_pkgs: int, no_line=False):
        if not no_line:
            print("-" * 72)

        print(
            "{}Found {}/{} suspicious packages in total.{}".format(
                Style.BRIGHT if USE_COLOR else "",
                suspicious,
                total_pkgs,
                Style.RESET_ALL if USE_COLOR else "",
            )
        )


def main():
    # Init messages
    msg = Msg()

    # Parse arguments
    args = parse_arguments()

    # NOTE: Verbosity levels:
    #   0 - print info about suspicious packages and totals
    #   1 - show info about used source files and pacman databases
    #   2 - show checking result for each package (suspicious or not) in a table
    #   3 - show data parsed from pacman output

    # Read packagers list
    input_file = finddatafile("share/asps/packagers-all.csv")
    if args.verbose:
        print("Loading packagers list from: {}".format(input_file))

    packagers = loadcsv(input_file)

    # Report database last update time
    if args.verbose:
        print(
            "Pacman databases were last updated at: {}".format(
                time.strftime(
                    "%Y-%m-%d %H:%M:%S %z",
                    time.localtime(
                        os.path.getmtime(get_newest_file("/var/lib/pacman/sync"))
                    ),
                )
            )
        )

    # Read package names from stdin
    pkgs_to_check = []
    if args.pacman_hook:
        for line in sys.stdin:
            pkgs_to_check.append(line.strip())

    # Run pacman and parse output
    def pacman(pacman_args: list) -> dict:
        """Helper function to get data from pacman"""
        env = dict(os.environ)
        env["LANG"] = "C"

        if args.pacman_hook:
            progress = False
        else:
            progress = True

        return parse_pacman_output(
            subprocess.check_output(["pacman"] + pacman_args, env=env),
            verbosity=args.verbose,
            progress=progress,
        )

    # Check packages in different modes (-p/-u, -a/none)
    suspicious = 0

    if args.pacman_hook or args.upgrades:
        if args.pacman_hook:
            local_ = pacman(["-Qi"])
            total = len(pkgs_to_check)

            # Exclude packages that are absent in the connected repos from pkgs_to_check
            # Without this you won't be able to install or upgrade locally built pkgs.
            pkgs_in_repos = list(pacman(["-Si"]).keys())
            pkgs_to_check = list(set(pkgs_to_check) & set(pkgs_in_repos))

            if pkgs_to_check:
                remote = pacman(["-Si"] + pkgs_to_check)
            else:
                remote = {}

        elif args.upgrades:
            local_ = pacman(["-Qiu"])
            remote = pacman(["-Si"] + list(local_.keys()))
            total = len(remote)

        for pkgname, new_packager in remote.items():
            result = is_trusted(new_packager, packagers)

            if args.verbose > 0:
                old_packager = local_.get(pkgname, "")

                # Is it a package that wasn't previously installed?
                if old_packager == "":
                    if args.verbose > 1:
                        msg.verbose(pkgname, new_packager, result)

                else:
                    # Report about packager change on upgrade
                    if args.verbose > 1 and old_packager != new_packager:
                        msg.verbose(
                            pkgname, old_packager + " -> " + new_packager, result
                        )
                    elif args.verbose > 1 and old_packager == new_packager:
                        msg.verbose(pkgname, old_packager, result)

                    if args.verbose > 0 and old_packager != new_packager:
                        msg.packager_has_changed(pkgname, old_packager, new_packager)

            # Report about suspicious package and count it
            if result < 0 and not args.no_unknown:
                msg.created_by_not_trusted(pkgname, new_packager)
                suspicious += 1
            elif result == 0:
                msg.created_by_russian(pkgname, new_packager)
                suspicious += 1

    else:
        pkgs = pacman(["-Si"]) if args.all_packages else pacman(["-Qi"])
        total = len(pkgs)

        for pkgname, packager in pkgs.items():
            result = is_trusted(packager, packagers)

            if args.verbose > 1:
                msg.verbose(pkgname, packager, result)

            # Report about suspicious package and count it
            if result < 0 and not args.no_unknown:
                msg.created_by_not_trusted(pkgname, packager)
                suspicious += 1
            elif result == 0:
                msg.created_by_russian(pkgname, packager)
                suspicious += 1

    # Report total
    msg.total(suspicious, total, no_line=args.pacman_hook)

    # Use non-zero exit code if any suspicious packages were found
    if suspicious > 0:
        sys.exit(1)


if __name__ == "__main__":
    main()

#!/usr/bin/env python
import csv
import os.path
import requests
import sys

from bs4 import BeautifulSoup

from asps import Packager, dumpcsv


# Constants
PEOPLE_URLS = [
    "https://archlinux.org/people/developers/",
    "https://archlinux.org/people/trusted-users/",
]
PACKAGES_URL = "https://archlinux.org/packages/"


def get_packager_data(soup) -> Packager:
    p = Packager()

    # base info
    for prop in ["name", "givenName", "familyName", "jobTitle"]:
        setattr(p, prop, soup.find("meta", itemprop=prop)["content"])

    # additional info (exc. country)
    def get_value(soup, text: str) -> str:
        keys = soup.find_all("th")
        for key in keys:
            if key.string == text:
                return key.find_next_sibling("td").text.strip()
        return ""

    # additional info (country)
    def get_country(soup, text: str) -> str:
        keys = soup.find_all("th")
        for key in keys:
            if key.string == text:
                result = key.find_next_sibling("td")
                try:
                    return result.span["title"]
                except TypeError:
                    return ""
        return ""

    p.alias = get_value(soup, "Alias:")
    p.email = get_value(soup, "Email:")
    p.otherContact = get_value(soup, "Other Contact:")
    p.pgpKey = get_value(soup, "PGP Key:")
    p.roles = get_value(soup, "Roles:")
    p.website = get_value(soup, "Website:")
    p.occupation = get_value(soup, "Occupation:")
    p.birthYear = get_value(soup, "Birth Year:")
    p.country = get_country(soup, "Location:")
    p.location = get_value(soup, "Location:")
    p.languages = get_value(soup, "Languages:")
    p.interests = get_value(soup, "Interests:")
    p.favoriteDistros = get_value(soup, "Favorite Distros:")

    return p


def main():
    packagers = []
    packagers_swu = {}

    swu_file = os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
        "packagers-standing-with-ukraine.csv",
    )

    if os.path.isfile(swu_file):
        with open(swu_file, "r") as fp:
            reader = csv.reader(fp, delimiter=",", quotechar='"')
            next(reader, None)  # skip header
            for row in reader:
                packagers_swu[row[0]] = row[1:]

    # Get /people pages
    for url in PEOPLE_URLS:
        print('Processing "{}"...'.format(url), flush=True)
        page = requests.get(url)
        content = page.content

        # Get users data
        soup = BeautifulSoup(content, "html.parser")

        bios_soup = soup.find(class_="arch-bio-entry")
        packagers_soup = bios_soup.find_all("tr", itemtype="http://schema.org/Person")

        for raw_packager_data in packagers_soup:
            p = get_packager_data(raw_packager_data)

            if p.name in packagers_swu.keys():
                p.standsWithUkraine = True
                p.proofURL1 = packagers_swu[p.name][0]
                p.proofURL2 = packagers_swu[p.name][1]
                p.proofURL3 = packagers_swu[p.name][2]
                p.proofURL4 = packagers_swu[p.name][3]

            packagers.append(p)

    # Recheck packagers
    print('Processing "{}"...'.format(PACKAGES_URL), flush=True)

    page = requests.get(PACKAGES_URL)
    content = page.content
    soup = BeautifulSoup(content, "html.parser")

    combobox_soup = soup.find("select", id="id_maintainer")

    for m_soup in combobox_soup.find_all("option"):
        maintainer = m_soup.text

        if maintainer in ["All", "Orphan"]:
            continue

        # Check if maintainer exists in Developers/Trusted users
        found = next((x.name for x in packagers if x.name == maintainer), None)
        if not found:
            print(
                'Packager "{}" is absent in Developers/Trusted Users list'.format(
                    maintainer
                ),
                file=sys.stderr,
            )

    # Print output
    outfile = os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
        "packagers-all.csv",
    )

    dumpcsv(packagers, outfile)

    print('Output written to "{}".'.format(outfile), flush=True)


if __name__ == "__main__":
    main()

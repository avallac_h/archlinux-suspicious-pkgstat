#!/usr/bin/env make

clean:
	rm -rvf .mypy_cache
	rm -rvf asps/__pycache__
	rm -rvf __pycache__
	rm -rvf asps.egg-info build dist
	rm -rvf pkg/pkg
	rm -vf  pkg/PKGBUILD.current
	rm -vf  pkg/archlinux-suspicious-pkgstat*.pkg.*
	rm -vf  archlinux-suspicious-pkgstat*.pkg.*

arch-pkg: clean
	cp -v pkg/PKGBUILD pkg/PKGBUILD.current
	cd pkg && makepkg -e -p PKGBUILD.current
	mv -v pkg/archlinux-suspicious-pkgstat*.pkg.* .


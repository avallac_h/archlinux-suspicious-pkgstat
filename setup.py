#!/usr/bin/env python3

import subprocess
import time

import setuptools

with open("README.mkd", "r") as fh:
    long_description = fh.read()

LAST_COMMIT_TIME = int(
    subprocess.check_output(["git", "log", "-1", '--format=%at']).decode().strip()
)
VERSION=time.strftime('%Y%m%d.%H%M', time.localtime(LAST_COMMIT_TIME))


setuptools.setup(
    name="asps",
    version=VERSION,
    author="Andrew Kotsyuba",
    author_email="avallach2000 (at) gmail.com",
    license="GPLv3",
    description=(
        "Check installed Arch Linux packages and report about "
        + "suspicious ones created by russian packagers"
    ),
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/avallac_h/archlinux-suspicious-pkgstat",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Operating System :: POSIX",
        (
            "License :: OSI Approved :: "
            "GNU Lesser General Public License v3 or later (LGPLv3+)"
        ),
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop",
        "Topic :: Utilities",
        "Topic :: Internet :: WWW/HTTP :: Indexing/Search",
        "Development Status :: 4 - Beta",
    ],
    python_requires=">=3.6",
    packages=setuptools.find_packages(),
    extras_require={
        "update lists of packagers": ["beautifulsoup4"],
        "produce colorized output": ["colorama"],
    },
    data_files=[("share/asps", ["packagers-all.csv", "packagers-standing-with-ukraine.csv"])],
    entry_points={
        "console_scripts": [
            "asps_report=asps.report:main",
            "asps_update=asps.update:main",
        ],
    },
)
